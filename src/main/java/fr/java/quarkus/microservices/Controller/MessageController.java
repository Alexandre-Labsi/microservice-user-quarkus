package fr.java.quarkus.microservices.Controller;

import java.awt.PageAttributes.MediaType;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.print.attribute.standard.Media;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;


import fr.java.quarkus.microservices.Model.Message;
import fr.java.quarkus.microservices.Service.MessageService;


@ApplicationScoped
@Path(value = "/messages")
public class MessageController {

    @Inject MessageService service;
    
    /**
     * Récupère la liste des messages
     * @return message
     */
    @GET
    @Consumes(value = "application/json")
    public Iterable<Message> getMessages(){
        return this.service.findAll();
    }

    /**
     * Crée un messge
     * @param message
     * @return message
     */
    @POST
    @Transactional
    public Message postMessage(Message message){
        return this.service.createOne(message);
    }

    /**
     * Modifie le message
     * @param id
     * @return
     */
    @PUT
    @Path(value = "/edit/{id}")
    @Transactional
    public Message editMessage(@PathParam Long id){
        Message message = this.service.editOne(id);

        return message;
    }

    /**
     * Supprime le message
     * @param id
     * @return
     */
    @DELETE
    @Path(value = "/delete/{id}")
    @Transactional
    public Message deleteMessage(@PathParam Long id){

        return this.service.deleteOne(id);
    }
}
