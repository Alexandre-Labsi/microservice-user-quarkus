package fr.java.quarkus.microservices.Service;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import fr.java.quarkus.microservices.Model.Message;

@ApplicationScoped
public class MessageService {
    
    @Inject EntityManager em;

    /**
     * Récupère les messages en bdd
     * @return messages[]
     * @exception Exception
     */
    public Iterable<Message> findAll() throws Exception {

        CriteriaBuilder builder = this.em.getCriteriaBuilder();
        CriteriaQuery<Message> query = builder.createQuery(Message.class);
        query.from(Message.class);

        Iterable<Message> messages = this.em.createQuery(query).getResultList();
        if(!messages.iterator().hasNext()){
            throw new Exception("NO_CONTENT");
        }
        
        return messages;
    }

    /**
     * Crée un message en bdd
     * @param message
     * @return message
     * @exception Exception
     */
    public Message createOne(Message message) throws Exception {
        if (message == null){
            throw new Exception("NOT_FOUND");
        }
        this.em.persist(message);
        return message;
    }

    /**
     * Récupère le message par son id et attend une modification 
     * puis update le message pour la bdd
     * @param id
     * @return message
     * @exception Exception
     */
    public Message editOne(Long id) throws Exception {
        if(id == null){
            throw new Exception("NOT_FOUND");
        }
        Message message = this.em.find(Message.class, id);
        if(message == null){
            throw new Exception("NOT_FOUND");
        }
        //ajoute des modifications
        this.em.merge(message);
        return message;
    }

    /**
     * Récupère le message par son id et le supprime de la bdd
     * @param id
     * @return message
     * @exception Exception
     */
    public Message deleteOne(Long id) throws Exception {
        if(id == null){
            throw new Exception("NOT_FOUND");
        }
        Message message = this.em.find(Message.class, id);
        if(message == null){
            throw new Exception("NOT_FOUND");
        }
        this.em.remove(message);

        return message;
    }
}
