package fr.java.quarkus.microservices.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data 
public class Message {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(name = "`from`", length = 50)
    private String from;

    @Column(name = "`to`", length = 50)
    private String to;

    @Column(name = "content", length = 200)
    private String content;

    @Column(name = "see")
    private Boolean see;

    Message(){
        this.see = false;
    }
}
