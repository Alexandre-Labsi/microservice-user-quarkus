# Quarkus - Microservice Messagerie

Dans de la cadre de l'intégration de Comiti pour un stage en entreprise de 2 mois, j'ai réçu un test a effectuer.

# Création d'un microservice

J'ai choisi la fonctionnalité "Messagerie", implémenté avec en Java avec le Framwork Quarkus.
Ce microservice utilise une base de donnée Mysql 5.7.

# Mise en place de l'environnement

Grâce à la documentation de Quarkus, j'ai mis en place le dépendences nécessaires au bon fonctionnement de mon projet.
- Resteasy pour la création du service REST
- Lombock pour les annotations getters & setters
- Hibernate ORM pour la manipulation des tables
- JDBC Mysql pour la liason avec la base de donnée

# Développement
Développement de l'organisation du code pour l'implémentation du microservice :
- Model -> un 'Message' contient à un 'expéditeur' , un 'destinataire', un 'contenu' et la possibilité qu'un message soit lu et non lu.
- Controller -> Récupération, Creation, Suppression, Modification.
- Repository -> Requêtes en base de donnée.
- Service -> Autres traitement et lever d'exceptions.

